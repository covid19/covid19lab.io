var colorAxis = {
    'activecases': ['#ffecc9', '#ff6000'],
    'confirmed': ['#d8d6ff', '#0d05ff'],
    'deaths': ['#ffc4c4', '#ff2727'],
    'recovered': [ '#c3ffc3', '#0f0' ],
    'fatality': [ '#ffc4ff', '#f0f' ],
    'markers': [ '#f00', '#f00' ]
};

var tooltips = {};

var tooltipsCity = {};

var regionsNice = {
    'world': 'World',
    '002': 'Africa',
    '150': 'Europe',
    '019': 'Americas',
    '142': 'Asia',
    '009': 'Oceania',
};

var modesNice = {
    'activecases': 'Active Cases',
    'confirmed': 'Confirmed Cases',
    'deaths': 'Deaths',
    'recovered': 'Recoveries',
    'fatality': 'Fatality Rate',
};

var superModesNice = {
    '24': 'Last 24 hours',
    'all': 'All Time',
    'capita': 'Per 1 million people'
};

var mapValueTitle = {
    "capitarecovered": "Recoveries per 1 million people",
    "capitadeaths": "Deaths per 1 million people",
    "capitaconfirmed": "Cases per 1 million people",
    "capitaactivecases": "Active Cases per 1 million people",
    "allfatality": "Fatality Rate",
    "allrecovered": "Recovered",
    "alldeaths": "Deaths",
    "allconfirmed": "Confirmed Cases",
    "allactivecases": "Active Cases",
    "24recovered": "New Recoveries",
    "24deaths": "New Deaths",
    "24confirmed": "New Cases",
};

var mapValueColumn = {
    "capitarecovered": "percRecovered",
    "capitadeaths": "percDeaths",
    "capitaconfirmed": "percCases",
    "capitaactivecases": "percActivecases",
    "allfatality": "fatality",
    "allrecovered": "recovered",
    "alldeaths": "deaths",
    "allconfirmed": "confirmed",
    "allactivecases": "activecases",
    "24recovered": "newrecoveries",
    "24deaths": "newdeaths",
    "24confirmed": "newcases",
};

var chart;

var graph;

var countryCities;

var countryCodeCities;

var chartinterval;

var initialLoad = 1;




function createCountryMap ( dataRaw, region ) {

    google.charts.setOnLoadCallback(function(){

      var dataGoogle = google.visualization.arrayToDataTable( dataRaw );

      var options = {
          colorAxis: {colors: colorAxis.markers },
          backgroundColor: '#181818',
          datalessRegionColor: '#ddd',
          magnifyingGlass: {enable: true, zoomFactor: 5.0},
          region: region,
          resolution: 'provinces',
          legend: 'none',
          sizeAxis: { minSize: 1, maxSize: 30 },
          displayMode: 'markers',
          tooltip: {isHtml: false, showTitle: true}
      };

      if ( dataRaw.length > 2 && dataRaw.length < 1000 ) {

          options.tooltip = {isHtml: true, showTitle: false};

      } else {

          options.sizeAxis.minSize = 0;

      }

      chart = new google.visualization.GeoChart(document.getElementById('map'));

      chart.draw(dataGoogle, options);

    });

}

function createCityTooltip( designation, city ) {

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {

        return '';

    }

    if ( tooltipsCity.hasOwnProperty( designation ) === true ) {

        return tooltipsCity[ designation ];

    }

    var html = '<h2>' + translateCountry( designation ) + '</h2>' +
        '<div><p>' + translateText( 'Confirmed Cases' ) + ':</p><span class="confirmed">' + city.c.toLocaleString() + '</span></div>' +
        '<div><p>' + translateText( 'Deaths' ) + ':</p><span class="deaths">' + city.d.toLocaleString() + '</span></div>' +
        '<div><p>' + translateText( 'Recoveries' ) + ':</p><span class="recovered">' + city.r.toLocaleString() + '</span></div>';

    tooltipsCity[ designation ] = html;

    return tooltipsCity[ designation ];

}

function createCitiesData() {

    var countryData = JSON.parse( this.responseText );

    var countryDataNum = Object.keys(countryData).length;

    var dataCities = [];

    if ( countryDataNum === 1 ) {

        dataCities[0] = [ 'Lat', 'Lon', 'City', 'Cases' ];

        for ( var designation in countryData ) {

            var cityData = [];
                cityData.push( countryData[ designation ].la );
                cityData.push( countryData[ designation ].lo );
                cityData.push( designation );
                cityData.push( countryData[ designation ].c );

            dataCities.push( cityData );

        }

    } else if ( countryDataNum < 1000 ) {

        dataCities[0] = [ 'Lat', 'Lon', 'Cases', {'type': 'string', 'role': 'tooltip', 'p': {'html': true}} ];

        for ( var designation in countryData ) {

            var cityData = [];
                cityData.push( countryData[ designation ].la );
                cityData.push( countryData[ designation ].lo );
                cityData.push( countryData[ designation ].c );
                cityData.push( createCityTooltip( designation, countryData[ designation ] ) );

            dataCities.push( cityData );

        }

    } else {

        dataCities[0] = [ 'Lat', 'Lon', 'City', 'Cases' ];

        for ( var designation in countryData ) {

            var cityData = [];
                cityData.push( countryData[ designation ].la );
                cityData.push( countryData[ designation ].lo );
                cityData.push( designation );
                cityData.push( countryData[ designation ].c );

            dataCities.push( cityData );

        }

    }

    if ( document.getElementById( 'country_name' ) !== null ) {

        chart.clearChart();

        populateCountryCounters( countryCities );

    }

    createCountryMap( dataCities, countryCodeCities );

}

function getCitiesData( countryId ) {

    if ( countryId.constructor === Array ) {

        countryCodeCities   = countryId[ 0 ];
        countryCities       = countryId[ 1 ];

    } else {

        countryCodeCities = countryId;

    }

    enableControlsCountry();

    var graphTotalReq = new XMLHttpRequest();
        graphTotalReq.addEventListener("load", createCitiesData);
        graphTotalReq.open("GET", "/data/cities/" + countryCodeCities + ".json");
        graphTotalReq.send();

}

function createMap ( dataNice, region, mode ) {

    if ( typeof chart !== 'undefined' ) {

        chart.clearChart();

    }

    if ( typeof region === 'undefined' ) {

        region = null;

    }

    google.charts.setOnLoadCallback(function(){

      var dataGoogle = google.visualization.arrayToDataTable( dataNice );

      var options = {
          colorAxis: {colors: colorAxis[ mode ] },
          backgroundColor: '#181818',
          datalessRegionColor: '#888',
          magnifyingGlass: {enable: true, zoomFactor: 5.0},
          region: region,
          legend: 'none',
          tooltip: {isHtml: true}
      };

      chart = new google.visualization.GeoChart(document.getElementById('map'));

      chart.draw(dataGoogle, options);

      google.visualization.events.addListener(chart, 'select', function( e ){

          var selectInfo = chart.getSelection();

          var row = selectInfo[0].row + 1;

          getCitiesData( [ dataNice[ row ][ 0 ].v, dataNice[ row ][ 0 ].c ] );

      });

    });

}

function createCountryTooltip( country ) {

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {

        return '';

    }

    if ( tooltips.hasOwnProperty( country ) === true ) {

        return tooltips[ country ];

    }

    var html = '<h2>' + translateCountry( country ) + '</h2>' +
        '<h3>' + translateText( 'All Time' ) + '</h3>' +
        '<div><p>' + translateText( 'Fatality Rate' ) + ':</p><span class="fatality">' + data[ country ].fatality + '%</span></div>' +
        '<div><p>' + translateText( 'Active Cases' ) + ':</p><span class="activecases">' + data[ country ].activecases.toLocaleString() + '</span></div>' +
        '<div><p>' + translateText( 'Confirmed Cases' ) + ':</p><span class="confirmed">' + data[ country ].confirmed.toLocaleString() + '</span></div>' +
        '<div><p>' + translateText( 'Deaths' ) + ':</p><span class="deaths">' + data[ country ].deaths.toLocaleString() + '</span></div>' +
        '<div><p>' + translateText( 'Recoveries' ) + ':</p><span class="recovered">' + data[ country ].recovered.toLocaleString() + '</span></div>' +
        '<h3>' + translateText( 'Last 24 hours' ) + '</h3>' +
        '<div><p>' + translateText( 'New Cases' ) + ':</p><span class="activecases">' + data[ country ].newcases.toLocaleString() + '</span></div>' +
        '<div><p>' + translateText( 'New Deaths' ) + ':</p><span class="deaths">' + data[ country ].newdeaths.toLocaleString() + '</span></div>' +
        '<div><p>' + translateText( 'New Recoveries' ) + ':</p><span class="recovered">' + data[ country ].newrecoveries.toLocaleString() + '</span></div>' +
        '<h3>' + translateText( 'Per 1 million people' ) + '</h3>' +
        '<div><p>' + translateText( 'Active Cases' ) + ':</p><span class="activecases">' + data[ country ].percActivecases + '</span></div>' +
        '<div><p>' + translateText( 'Confirmed Cases' ) + ':</p><span class="confirmed">' + data[ country ].percCases + '</span></div>' +
        '<div><p>' + translateText( 'Deaths' ) + ':</p><span class="deaths">' + data[ country ].percDeaths + '</span></div>' +
        '<div><p>' + translateText( 'Recoveries' ) + ':</p><span class="recovered">' + data[ country ].percRecovered + '</span></div>' +
        '<span class="legend"><b>*</b> ' + translateText( 'Click on the country for more information' ) + '</span>';

    tooltips[ country ] = html;

    return tooltips[ country ];

}

function formCountryData( country, value ) {

    if ( value < 0 ) { value = 0; }

    var countryData = [];

    countryData.push( {v:data[ country ].g,f:'',c:country} );

    countryData.push( value );

    if ( ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {

        countryData.push( '' );

    } else {

        countryData.push( createCountryTooltip( country ) );

    }

    return countryData;

}

function formData( supermode, mode ) {

    if ( supermode === 'capita' && mode === 'fatality' ) {

        supermode = "all";

    } else if ( supermode === '24' && mode === 'fatality' ) {

        supermode = 'all';

    } else if ( supermode === '24' && mode === 'activecases' ) {

        supermode = 'all';

    }

    var dataRaw = [];

    dataRaw[0] = [ 'Country', mapValueTitle[ supermode + mode ], {'type': 'string', 'role': 'tooltip', 'p': {'html': true}} ];

    for ( var country in data ) {

        if ( country === "Cruise Ship (Princess Diamond)" ) { continue; }
        if ( country === "Channel Islands" ) { continue; }
        if ( country === "Saint Martin (French part)" ) { continue; }
        if ( country === "Tanzania" ) { continue; }
        if ( country === "Tanzania, United Republic of" ) { continue; }
        if ( country === "San Marino" ) { continue; }
        if ( country === "Faroe Islands" ) { continue; }
        if ( country === "Holy See" ) { continue; }
        if ( country === "" ) { continue; }
        if ( country === "MSZaandam" ) { continue; }
        if ( country === "CaribbeanNetherlands" ) { continue; }

        var countryData = formCountryData( country, data[ country ][ mapValueColumn[ supermode + mode ] ] );

        dataRaw.push( countryData );

    }

    return dataRaw;

}

function enableControlsCountry() {

    document.getElementById( 'controlsCountry' ).classList.remove( 'hidden' );

    document.getElementById( 'controls' ).classList.add( 'hidden' );

}

function disableControlsCountry() {

    document.getElementById( 'controlsCountry' ).classList.add( 'hidden' );

    document.getElementById( 'controls' ).classList.remove( 'hidden' );

}

function getUrlParameter(name) {

    name            = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex       = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results     = regex.exec(location.search);

    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function detectEnvironment() {

    if ( initialLoad === 1 ) {

        initialLoad = 0;

    } else {

        return false;

    }

    var region = getUrlParameter( 'region' ); //console.log( region );
    var mode = getUrlParameter( 'mode' ); //console.log( mode );
    var supermode = getUrlParameter( 'supermode' ); //console.log( supermode );
    var country = getUrlParameter( 'country' );

    return [ region, mode, supermode, country ];

}

function informLegend() {

    var items = document.querySelectorAll( '#controls .item' );
    var itemsNum = items.length;

    for ( var q = 0 ; q < itemsNum ; q++ ) {

        items[ q ].classList.remove( 'active' );

    }

    var environment = detectEnvironment(); //console.log( environment );
    var region;
    var mode;
    var supermode;
    var country;

    if ( environment === false ) {

        region      = document.querySelector( '#controls .region p' ).getAttribute( 'data-region' );
        mode        = document.querySelector( '#controls .mode p' ).getAttribute( 'data-mode' );
        supermode   = document.querySelector( '#controls .supermode p' ).getAttribute( 'data-supermode' );
        country     = 'world';

    } else {

        region      = environment[ 0 ];
        mode        = environment[ 1 ];
        supermode   = environment[ 2 ];
        country     = environment[ 3 ];

        if ( country === '' ) {

            country = 'world';

        }

        if ( region !== '' ) {

            document.querySelector( '#controls .region p' ).setAttribute( 'data-region', region );
            document.querySelector( '#controls .region p' ).setAttribute( 'data-transtext', regionsNice[ region ] );
            document.querySelector( '#controls .region p' ).textContent = translateText( regionsNice[ region ] );

        } else {

            region = document.querySelector( '#controls .region p' ).getAttribute( 'data-region' );

        }

        if ( mode !== '' ) {

            document.querySelector( '#controls .mode p' ).setAttribute( 'data-mode', mode );
            document.querySelector( '#controls .mode p' ).setAttribute( 'data-transtext', modesNice[ mode ] );
            document.querySelector( '#controls .mode p' ).textContent = translateText( modesNice[ mode ] );

        } else {

            mode = document.querySelector( '#controls .mode p' ).getAttribute( 'data-mode' );

        }

        if ( supermode !== '' ) {

            document.querySelector( '#controls .supermode p' ).setAttribute( 'data-supermode', supermode );
            document.querySelector( '#controls .supermode p' ).setAttribute( 'data-transtext', superModesNice[ supermode ] );
            document.querySelector( '#controls .supermode p' ).textContent = translateText( superModesNice[ supermode ] );

        } else {

            supermode = document.querySelector( '#controls .supermode p' ).getAttribute( 'data-supermode' );

        }

    }

    document.querySelector( '#controls .region .list div[data-region="' + region + '"]' ).classList.add( 'active' );
    document.querySelector( '#controls .mode .list div[data-mode="' + mode + '"]' ).classList.add( 'active' );
    document.querySelector( '#controls .supermode .list div[data-supermode="' + supermode + '"]' ).classList.add( 'active' );

    var superModes      = document.querySelectorAll( '#controls .supermode .list div' );
    var superModesNum   = superModes.length;

    var flagInactive = 0;

    for ( var i = 0 ; i < superModesNum ; i++ ) {

        if ( superModes[ i ].classList.contains( mode ) === false ) {

            superModes[ i ].style.display = 'none';

            if ( superModes[ i ].classList.contains( 'active' ) === false ) {

                superModes[ i ].classList.remove( 'active' );
                flagInactive = 1;

            }

        } else {

            superModes[ i ].style.display = 'block';

        }

    }

    if ( flagInactive === 1 ) {

        document.querySelector( '#controls .supermode .list div[data-supermode="all"]' ).classList.add( 'active' );

    }

    return { region: region, mode: mode, supermode: supermode, type: country };

}

function resetMapType() {

    document.querySelector( '#controls .region p' ).setAttribute( 'data-region', 'world' );
    document.querySelector( '#controls .region p' ).setAttribute( 'data-transtext', 'World' );
    document.querySelector( '#controls .region p' ).textContent = translateText( 'World' );

    document.querySelector( '#controls .mode p' ).setAttribute( 'data-mode', 'activecases' );
    document.querySelector( '#controls .mode p' ).setAttribute( 'data-transtext', 'Active Cases' );
    document.querySelector( '#controls .mode p' ).textContent = translateText( 'Active Cases' );

    document.querySelector( '#controls .supermode p' ).setAttribute( 'data-supermode', 'all' );
    document.querySelector( '#controls .supermode p' ).setAttribute( 'data-transtext', 'All Time' );
    document.querySelector( '#controls .supermode p' ).textContent = translateText( 'All Time' );

}

function click_backtoworld( event ) {

    disableControlsCountry();

    if ( document.getElementById( 'country_name' ) !== null ) {

        unPopulateCountryCounters();

    }

    resetMapType();

    var maptype = informLegend();

    createMap( formData( maptype.supermode, maptype.mode ), maptype.region, maptype.mode );

}

function click_item( event ) {

    var dataset = event.currentTarget.dataset;
    var target = event.currentTarget.parentElement.parentElement.querySelector( 'p' );

    for ( var attr in dataset ) {

        target.setAttribute( 'data-' + attr, dataset[ attr ] );

    }

    target.textContent = event.currentTarget.textContent;

    disableHoveredLegends();

    var maptype = informLegend();

    createMap( formData( maptype.supermode, maptype.mode ), maptype.region, maptype.mode );

}

function bindItems() {

    var items = document.querySelectorAll( '#controls .item' );
    var itemsNum = items.length;

    for ( var i = 0 ; i < itemsNum ; i++ ) {

        items[ i ].addEventListener( 'click', click_item );

    }

}

function disableHoveredLegends() {

    var columns = document.querySelectorAll( '#controls > div' );
    var columnsNum = columns.length;

    for ( var i = 0 ; i < columnsNum ; i++ ) {

        columns[ i ].classList.remove( 'hovered' );

    }

}

function click_legend( event ) {

    var columns = document.querySelectorAll( '#controls > div' );
    var columnsNum = columns.length;

    if ( event.currentTarget.parentElement.classList.contains( 'hovered' ) ) {

        for ( var i = 0 ; i < columnsNum ; i++ ) {

            columns[ i ].classList.remove( 'hovered' );

        }

    } else {

        for ( var q = 0 ; q < columnsNum ; q++ ) {

            columns[ q ].classList.remove( 'hovered' );

        }

        event.currentTarget.parentElement.classList.add( 'hovered' );

    }

}

function bindLegends() {

    var legends = document.querySelectorAll( '#controls p' );
    var legendsNum = legends.length;

    for ( var i = 0 ; i < legendsNum ; i++ ) {

        legends[ i ].addEventListener( 'click', click_legend );

    }

}

function drawMap() {

    var maptype = informLegend();

    if ( maptype.type === 'world' ) {

        createMap( formData( maptype.supermode, maptype.mode ), maptype.region, maptype.mode );

    } else {

        getCitiesData( maptype.type )

    }

    if ( document.getElementById('graph_chart') !== null ) {

        var script_chart = document.createElement('script');

        script_chart.onload = function () {

            get_graphTotal();

        };

        script_chart.src = "/js/chart.js";

        document.head.appendChild( script_chart );

    }

}




var script_gstatic = document.createElement('script');

script_gstatic.onload = function () {

    chartinterval = window.setInterval( function(){

        google.charts.load('current', {
            'packages':['geochart'],
            'mapsApiKey': 'AIzaSyAh3ImWg6RwxMtYAKSPbGRW4SMZ7e0OwL8'
        });

        google.charts.setOnLoadCallback( drawMap );

        clearInterval( chartinterval );

    }, 1000 );

};

script_gstatic.src = "https://www.gstatic.com/charts/loader.js";

document.head.appendChild( script_gstatic );

bindItems();

bindLegends();

document.getElementById( 'controlsCountry' ).addEventListener( 'click', click_backtoworld );
